#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h> //open,creat
#include <sys/types.h> //open
#include <sys/stat.h>
#include <errno.h> //perror, errno
#include <string.h>

typedef struct {
	char nev[10];
	char lakhely[10];
	char napok[10][60];
} munkas;

munkas munkasok[10];
int munkasokSzama;
int szukseglet[5] = { 3,2,4,1,3 };
char napok[5][10] = { "hetfo","kedd","szerda","csutortok","pentek" };

void beolvas(char* fileName) {
	FILE* f;
	f = fopen(fileName, "r");
	char line[160];
	char *t;
	int i = 0;
	while (!feof(f)) {
		int j = 0;
		int nap = 0;
		fgets(line, sizeof(line), f);
		for (size_t i = 0; i < 160; i++)
		{
			if (line[i] == '\n') line[i] = ' ';
		}
		t = strtok(line, " ");
		while (t != NULL) {
			if (j == 0) {
				strcpy(munkasok[i].nev,t);
				//printf("%s", munkasok[i].nev);
			}
			else if (j == 1) {
				strcpy(munkasok[i].lakhely, t);
				//printf("%s", munkasok[i].lakhely);
			} 
			else {
				strcpy(munkasok[i].napok[nap], t);
				nap++;
			}
			j++;
			t = strtok(NULL, " ");
		}
		i++;
	}
	munkasokSzama = i;
	//close(f);
}

void kiir() {
	for (int i = 0; i < munkasokSzama; i++)
	{
		printf("%s %s\n", munkasok[i].nev, munkasok[i].lakhely);
			
		//int j = 0;
		/*while (munkasok[i].napok[j] != NULL)
		{
			printf(munkasok[i].napok[j]);
			j++;
		}*/
		for (int j = 0; j < 10; j++)
		{
			printf("%s ",munkasok[i].napok[j]);
		}
		printf("\n");
	}
}

int countOfChars(char* arr) {
	int c = 0;
	while (arr[c] != '\0')
	{
		c++;
	}
	return c;
}

void kiirFile(char* fileName) {
	FILE* f;
	f = fopen("asd.txt", "w");
	for (size_t i = 0; i < munkasokSzama; i++)
	{
		fwrite(munkasok[i].nev, sizeof(char), countOfChars(munkasok[i].nev), f);
		fwrite(" ", sizeof(char), 1, f);
		fwrite(munkasok[i].lakhely, sizeof(char), countOfChars(munkasok[i].lakhely), f);
		fwrite(" ", sizeof(char), 1, f);
		for (size_t j = 0; j < 10; j++)
		{
			printf("%d ", j);
			fwrite(munkasok[i].napok[j], sizeof(char), countOfChars(munkasok[i].napok[j]), f);
			fwrite(" ", sizeof(char), 1, f);
		}
		fwrite("\n", sizeof(char), 1, f);
	}
}

void beosztas() {
	for (size_t i = 0; i < 5; i++)
	{	
		int talalt = 0;
		printf("%d. nap:\n",i);
		for (size_t j = 0; j < munkasokSzama && talalt < szukseglet[i]; j++)
		{
			for (size_t k = 0; k < 10; k++)
			{
				//printf("%s %s\n", munkasok[j].napok[k], napok[i]);
				if (strcmp(munkasok[j].napok[k], napok[i]) == 0) {
					talalt++;
					printf("%s ",munkasok[j].nev);
				}
			}
		}
		printf("\n");
	}
}

void modosit() {
	for (int i = 0; i < munkasokSzama; i++)
	{
		printf("%d %s %s \n", i + 1, munkasok[i].nev, munkasok[i].lakhely);
	}
	printf("Hanyadik munk�st akarod m�dos�tani?\n");
	int d;
	scanf("%d", &d);
	printf("Nevet(N) vagy lakhelyet(L) szeretn�l m�dos�tani?\n");
	char c;
	scanf("%c", &c);
	scanf("%c", &c);
	if (c == 'N') {
		printf("Add meg az �j nevet!: ");
		scanf("%s", munkasok[d - 1].nev);
	}
	else if (c == 'L') {
		printf("Add meg az �j lakhelyet!: ");
		scanf("%s", munkasok[d - 1].lakhely);
	}
	printf("A %d. munk�s �j adatai:\n", d);
	printf("%d %s %s \n", d, munkasok[d - 1].nev, munkasok[d - 1].lakhely);
	kiirFile("asd.tx");
}

void menu() {
	printf("V�lassz egy opci�t! (1) - Jelent�s; (2) - M�dos�t�s; (3) - Exit\n");
	int c;
	scanf("%d", &c);
	if (c == 1) {
		beosztas();
	}
	else if (c == 2) {
		modosit();
	}
	else if (c == 3) {
		exit(0);
	}
}


int main(int argc, char** argv){
	beolvas(argv[1]);
	while (1) {
		menu();
	}
}